package com.kuanyu.interview;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.ui.AppBarConfiguration;

import com.kuanyu.interview.databinding.ActivityMainBinding;
import com.kuanyu.interview.fragment.ChatFragment;
import com.kuanyu.interview.fragment.ExploreFragment;
import com.kuanyu.interview.fragment.PhoneFragment;
import com.kuanyu.interview.fragment.WalletFragment;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        fragmentManager = getSupportFragmentManager();
        binding.bottomBar.setOnItemSelectedListener(item -> {
            fragmentTransaction = fragmentManager.beginTransaction();
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.phone:
                    fragment = new PhoneFragment();
                    //fragment覆蓋(replace)，使用blankFragment的class
                    fragmentTransaction.replace(R.id.fragmentContainerView, fragment);
                    fragmentTransaction.commit();
                    break;
                case R.id.chat:
                    fragment = new ChatFragment();
                    //fragment覆蓋(replace)，使用blankFragment的class
                    fragmentTransaction.replace(R.id.fragmentContainerView, fragment);
                    fragmentTransaction.commit();
                    break;
                case R.id.explore:
                    fragment = new ExploreFragment();
                    //fragment覆蓋(replace)，使用blankFragment的class
                    fragmentTransaction.replace(R.id.fragmentContainerView, fragment);
                    fragmentTransaction.commit();
                    break;
                case R.id.wallet:
                    fragment = new WalletFragment();
                    //fragment覆蓋(replace)，使用blankFragment的class
                    fragmentTransaction.replace(R.id.fragmentContainerView, fragment);
                    fragmentTransaction.commit();
                    break;
            }
            return true;
        });
        binding.bottomBar.setSelectedItemId(R.id.phone);

    }
}