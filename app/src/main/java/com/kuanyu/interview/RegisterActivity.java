package com.kuanyu.interview;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kuanyu.interview.api.APIService;
import com.kuanyu.interview.api.LoginResult;
import com.kuanyu.interview.api.QueryUserInfoResult;
import com.kuanyu.interview.api.RegisterResult;
import com.kuanyu.interview.api.UserInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private final String TAG = "Register";
    private EditText account;
    private EditText password;
    private EditText firstName;
    private EditText lastName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        account = findViewById(R.id.account);
        password = findViewById(R.id.password);
        final Button registerBtn = findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(view -> {
            APIService service = ((MyApplication)getApplication()).getService();
            String accountStr = account.getText().toString();
            String firstNameStr = firstName.getText().toString();
            String lastNameStr = lastName.getText().toString();
            String passwordStr = password.getText().toString();
            service.register(accountStr, firstNameStr, lastNameStr, passwordStr).enqueue(new Callback<RegisterResult>() {

                @Override
                public void onResponse(Call<RegisterResult> call, Response<RegisterResult> response) {
                    if (response.isSuccessful()) {
                        RegisterResult result = response.body();
                        int status = result.getStatus();
                        Log.i(TAG, "Status: " + status);
                        if (status != 0) {
                            String errorMessage = result.getErrorMessage();
                            Log.i(TAG, "Error message: " + errorMessage);
                            failureProcess(errorMessage);
                        } else {
                            String token = result.getToken();
                            Log.i(TAG, "Token: " + token);
                            service.getUserInfo(token).enqueue(new Callback<QueryUserInfoResult>() {
                                @Override
                                public void onResponse(Call<QueryUserInfoResult> call, Response<QueryUserInfoResult> response) {
                                    if (response.isSuccessful()) {
                                        QueryUserInfoResult result = response.body();
                                        int status = result.getStatus();
                                        Log.i(TAG, "Status: " + status);
                                        if (status != 0) {
                                            String errorMessage = result.getErrorMessage();
                                            Log.i(TAG, "Error message: " + errorMessage);
                                            failureProcess(errorMessage);
                                        } else {
                                            UserInfo info = result.getUserInfo();
                                            String userName = info.getLast_name() + " " + info.getFirst_name();
                                            Log.i(TAG, "User name: " + userName);
                                            ((MyApplication)getApplication()).setUserName(userName);
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    } else {
                                        failureProcess("Call API failure with " + response.code());
                                    }
                                }

                                @Override
                                public void onFailure(Call<QueryUserInfoResult> call, Throwable t) {
                                    failureProcess("Call getUserInfo API failure with " + response.code());
                                }
                            });
                        }
                    } else {
                        failureProcess("Call register API failure with " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<RegisterResult> call, Throwable throwable) {

                    throwable.printStackTrace();
                    failureProcess(throwable.getMessage());
                }
            });
        });
    }

    private void failureProcess(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
