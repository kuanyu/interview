package com.kuanyu.interview;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kuanyu.interview.api.APIService;
import com.kuanyu.interview.api.LoginResult;
import com.kuanyu.interview.api.QueryUserInfoResult;
import com.kuanyu.interview.api.UserInfo;
import com.kuanyu.interview.databinding.LoginBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private final String TAG = "Login";
    private LoginBinding binding;
    private EditText account;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = LoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        account = binding.account;
        password = binding.password;
        binding.loginBtn.setOnClickListener(view -> {
            APIService service = ((MyApplication)getApplication()).getService();
            String accountStr = account.getText().toString();
            String passwordStr = password.getText().toString();
            service.login(accountStr, passwordStr).enqueue(new Callback<LoginResult>() {

                @Override
                public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
                    if (response.isSuccessful()) {
                        LoginResult result = response.body();
                        int status = result.getStatus();
                        Log.i(TAG, "Status: " + status);
                        if (status != 0) {
                            String errorMessage = result.getErrorMessage();
                            Log.i(TAG, "Error message: " + errorMessage);
                            failureProcess(errorMessage);
                        } else {
                            String token = result.getToken();
                            Log.i(TAG, "Token: " + token);
                            service.getUserInfo(token).enqueue(new Callback<QueryUserInfoResult>() {
                                @Override
                                public void onResponse(Call<QueryUserInfoResult> call, Response<QueryUserInfoResult> response) {
                                    if (response.isSuccessful()) {
                                        QueryUserInfoResult result = response.body();
                                        int status = result.getStatus();
                                        Log.i(TAG, "Status: " + status);
                                        if (status != 0) {
                                            String errorMessage = result.getErrorMessage();
                                            Log.i(TAG, "Error message: " + errorMessage);
                                            failureProcess(errorMessage);
                                        } else {
                                            UserInfo info = result.getUserInfo();
                                            String userName = info.getLast_name() + " " + info.getFirst_name();
                                            Log.i(TAG, "User name: " + userName);
                                            ((MyApplication)getApplication()).setUserName(userName);
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(intent);
                                        }
                                    } else {
                                        failureProcess("Call API failure with " + response.code());
                                    }
                                }

                                @Override
                                public void onFailure(Call<QueryUserInfoResult> call, Throwable t) {
                                    failureProcess("Call getUserInfo API failure with " + response.code());
                                }
                            });
                        }
                    } else {
                        failureProcess("Call register API failure with " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<LoginResult> call, Throwable throwable) {

                    throwable.printStackTrace();
                    failureProcess(throwable.getMessage());
                }
            });
        });
        final Button registerBtn = findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        });
    }
    private void failureProcess(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
