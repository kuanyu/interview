package com.kuanyu.interview.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.kuanyu.interview.R;
import com.kuanyu.interview.databinding.FragmentChatBinding;
import com.kuanyu.interview.databinding.FragmentCoinBinding;

import java.util.Random;

public class CoinFragment extends Fragment {

    private FragmentCoinBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionInflater inflater = TransitionInflater.from(requireContext());
        setEnterTransition(inflater.inflateTransition(R.transition.slide_right));
//        setExitTransition(inflater.inflateTransition(R.transition.slide_left));
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentCoinBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int cardCount = getRandomNumber(6);
        for(int i =0;i<cardCount;i+=1) {
            View cardView = inflater.inflate(R.layout.view_card, null);
            TextView expiresDay = cardView.findViewById(R.id.expires);
            int expires = getRandomNumber(365);
            expiresDay.setText(expires + " days");
            ProgressBar progress = cardView.findViewById(R.id.expires_progress);
            int percentage = (int)((float) expires / 365.0f);
            progress.setProgress(percentage);
            binding.cardList.addView(cardView);
        }

    }

    private int getRandomNumber(int num) {
        Random random = new Random();
        return random.nextInt(num) + 1;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}