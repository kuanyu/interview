package com.kuanyu.interview.fragment;

import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;
import com.kuanyu.interview.MyApplication;
import com.kuanyu.interview.R;
import com.kuanyu.interview.databinding.FragmentWalletBinding;

public class WalletFragment extends Fragment {

    private FragmentWalletBinding binding;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentWalletBinding.inflate(inflater, container, false);
        fragmentManager = getParentFragmentManager();
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TabLayout.Tab tab = binding.tabLayout.getTabAt(1);
        String orgTabText = tab.getText().toString();
        tab.setText(orgTabText + "(4)");
        tab.getOrCreateBadge();
        String userName = ((MyApplication)getActivity().getApplication()).getUserName();
        binding.username.setText(userName);
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabChange();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });
        onTabChange();
    }

    private void onTabChange() {
        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = new CoinFragment();
        fragmentTransaction.replace(R.id.coinFragmentContainer, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}