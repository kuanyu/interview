package com.kuanyu.interview.api;

import org.json.JSONObject;

public class QueryUserInfoResult {
    private int status;
    private UserInfo user_info;
    private String error;

    public QueryUserInfoResult(int status, UserInfo user_info, String error) {
        this.status = status;
        this.user_info = user_info;
        this.error = error;
    }


    public UserInfo getUserInfo() {
        return user_info;
    }

    public int getStatus() {
        return status;
    }

    public String getErrorMessage() {
        return error;
    }
}
