package com.kuanyu.interview.api;

public class UserInfo {
    private String mail;
    private String first_name;
    private String last_name;

    public UserInfo(String mail, String first_name, String last_name) {
        this.mail = mail;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public String getMail() {
        return mail;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }
}
