package com.kuanyu.interview.api;

public class RegisterResult {
    private String error;
    private int status;
    private String token;

    public RegisterResult(int status, String error, String token) {
        this.status = status;
        this.token = token;
        this.error = error;
    }


    public String getToken() {
        return token;
    }


    public int getStatus() {
        return status;
    }

    public String getErrorMessage() {
        return error;
    }
}
