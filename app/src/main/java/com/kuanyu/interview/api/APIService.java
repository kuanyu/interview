package com.kuanyu.interview.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {
    @FormUrlEncoded
    @POST("interview.php?api=login")
    Call<LoginResult> login(@Field("mail") String mail, @Field("passwd") String password);

    @FormUrlEncoded
    @POST("interview.php?api=register")
    Call<RegisterResult> register(@Field("mail") String mail, @Field("first_name") String firstName, @Field("last_name") String lastName, @Field("passwd") String password);

    @GET("interview.php?api=get_user_info")
    Call<QueryUserInfoResult> getUserInfo(@Query("token") String token);
}
