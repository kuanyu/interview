package com.kuanyu.interview.api;

public class LoginResult {
    private String error;
    private int status;
    private String token;

    public LoginResult(int status, String token, String error) {
        this.status = status;
        this.token = token;
        this.error = error;
    }


    public String getToken() {
        return token;
    }


    public int getStatus() {
        return status;
    }

    public String getErrorMessage() {
        return error;
    }
}
